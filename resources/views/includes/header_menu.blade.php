<div class="mobile-menu-overlay"></div>
<div class="mobile-menu-body">
    <div class="block-close">
        <a class="close"></a>
    </div>
    <div class="block-menu">
        <ul class="menu">
            {{--menu from db--}}
            @foreach($data['menu'] as $menu_item)
                <li>
                    <a href="{{$menu_item['url']}}"
                    @if($menu_item['blank'])
                    target="_blank"
                    @endif
                    >{{$menu_item['name']}}</a>
                </li>
            @endforeach
            {{--end menu from db--}}
        </ul>
        <ul class="lang">
            {{--langs from db--}}
            @foreach($data['langs'] as $lang)
                @if($lang['code'] == $data['locale'])
                    <li class="active">
                        <a href="/{{$lang['code']}}"><img src={{asset('/').$lang['flag_path']}} alt={{$lang['code']}}>{{$lang['code']}}</a>
                    </li>
                @else
                    <li>
                        <a href="/{{$lang['code']}}"><img src={{asset('/').$lang['flag_path']}} alt={{$lang['code']}}>{{$lang['code']}}</a>
                    </li>
                @endif
            @endforeach
            {{--end langs from db--}}
        </ul>
    </div>
</div>
<header
    @if($main)
    class="header"
    @else
    class="header always-white"
    @endif
>
    <a class="mobile-menu-button"></a>
    <div class="container">
        <div class="logo">
            <a href="/{{$data['locale']}}"></a>
        </div>
        <ul class="menu">
            {{--menu from db--}}
            @foreach($data['menu'] as $menu_item)
                <li>
                    <a href="{{$menu_item['url']}}"
                    @if($menu_item['blank'])
                    target="_blank"
                    @endif
                    >{{$menu_item['name']}}</a>
                </li>
            @endforeach
            {{--end menu from db--}}
        </ul>
        <div class="lang">
            {{--langs from db--}}
            @foreach($data['langs'] as $lang)
                @if($lang['code'] == $data['locale'])
                    <a href="/{{$lang['code']}}" class="current">
                        <img src="{{asset('/').$lang['flag_path']}}" alt={{$lang['code']}}>{{$lang['code']}}
                    </a>
                @endif
            @endforeach
            <ul class="list">
                @foreach($data['langs'] as $lang)
                    <li><a href="/{{$lang['code']}}"><img src="{{asset('/').$lang['flag_path']}}" alt={{$lang['code']}}>{{$lang['code']}}</a></li>
                @endforeach
            </ul>
            {{--end langs from db--}}
        </div>
    </div>
</header>
