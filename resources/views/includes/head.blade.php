<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/images/favicon-32x32.png')}}">
    <link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}">
    <meta name="keywords" content={{$data['keywords']}}>
    <meta name="description" content={{$data['description']}}>
    <title>{{$data['title']}}</title>
    <meta name="format-detection" content="telephone=no">
{{--    <meta name="csrf-token" content="{{csrf_token()}}">--}}
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
    <link href="{{url('/')}}/css/style.css" rel="stylesheet" type="text/css">
    @if($main)
    <link href="{{url('/')}}/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
    @endif
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{url('/')}}/js/owl.carousel.js"></script>
    <script src="{{url('/')}}/js/init.js"></script>
</head>
