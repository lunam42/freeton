<footer class="footer">
    <div class="container">
        <p>
            <a href="mailto:freeton@info.com" class="email">freeton@info.com</a>
        </p>
        <div class="socials">
            <a href="#" class="social-1"></a>
            <a href="#" class="social-2"></a>
            <a href="#" class="social-3"></a>
        </div>
        <p>{{__('strings.copyright')}}</p>
    </div>
</footer>
