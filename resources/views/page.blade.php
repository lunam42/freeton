<!doctype html>
<html lang="{{$data['locale']}}">
@include('includes/head', ['data' => $data['page'], 'main' => false])
@include('includes/header_menu', ['menu' => $data['menu'],'langs' => $data['langs'], 'locale' => $data['locale'], 'main' => false])
<body>
<section class="inner-content">
    <div class="container">
        <div class="pages-tree">
            <a href="{{url("/{$data['locale']}")}}">Home</a>
            {{$data['page']['url']}}
        </div>
        <div class="headline">
            <div class="main">{{$data['page']['headline']}}</div>
            <div class="shadowed">{{$data['page']['headline']}}</div>
        </div>
        <div class="static-page-content">
            {{$data['page']['content']}}
{{--            <div class="video">--}}
{{--                <a href="#" class="play-button"></a>--}}
{{--            </div>--}}
        </div>
    </div>
</section>
@include('includes/footer')
</body>
</html>
