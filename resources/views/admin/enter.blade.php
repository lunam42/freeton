<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>login</title>
    <link href="{{asset('/css/app.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('/js/app.js')}}"></script>
</head>
<body>
<div class="container min-vh-100 d-flex flex-column justify-content-center align-items-center">
    @isset($error)
        <h3 class="text-danger">{{$error}}</h3>
    @endisset
    <form class="col-10 col-sm-7 col-md-6 col-lg-4 bg-white p-4 border rounded-lg shadow"
          method="POST" action="{{route('enter')}}">
        @csrf
        <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="username">
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="password">
        </div>
        <button type="submit" class="btn btn-primary mx-auto d-block shadow">LOGIN</button>
    </form>
</div>
</body>
</html>
