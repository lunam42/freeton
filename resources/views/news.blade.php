<!doctype html>
<html lang="{{$data['locale']}}">
@include('includes/head', ['data' => $data['page'], 'main' => false])
@include('includes/header_menu', ['menu' => $data['menu'],'langs' => $data['langs'], 'locale' => $data['locale'], 'main' => false])
<body>
<section class="inner-content">
    <div class="container">
        <div class="pages-tree">
            <a href="{{url("/{$data['locale']}")}}">{{__('strings.home')}}</a>
            {{__('strings.news')}}
        </div>
        <div class="headline">
            <div class="main">{{$data['page']['headline']}}</div>
            <div class="shadowed">{{$data['page']['headline']}}</div>
        </div>
        <div class="news-list">
            @foreach($data['news'] as $news_item)
            <div class="item">
                <a href="{{$news_item['url']}}">
                    <div class="image">
                        <img src="{{$news_item['img_path']}}" alt="{{$news_item['title']}}">
                    </div>
                    <div class="details">
                        <p>
                            <b>{{$news_item['title']}}</b> {{$news_item['description']}}
                        </p>
                        <div class="date">{{$news_item['updated_at']}}</div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
{{--        <div class="pager">--}}
{{--            <a class="active">1</a>--}}
{{--            <a href="#">2</a>--}}
{{--        </div>--}}
    </div>
</section>
@include('includes/footer')
</body>
</html>
