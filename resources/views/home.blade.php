<!doctype html>
<html lang="{{$data['locale']}}">
@include('includes/head', ['data' => $data['page'], 'main' => true])
<body>
@include('includes/header_menu', ['menu' => $data['menu'],'langs' => $data['langs'], 'locale' => $data['locale'], 'main' => true])
@isset($data['slider_items'])
<section class="main-banner">
    <div class="container">
        <div class="main-banner-slider-body">
            <div class="main-banner-slider">
{{--sliders from db--}}
            @foreach($data['slider_items'] as $slider)
                <div>
                    <div class="title">{{$slider['heading']}}</div>
                    <p>
                        {{$slider['text']}}
                    </p>
                    @if($slider['button_url'] && $slider['button_text'])
                    <a href="{{$slider['button_url']}}" target="_blank" class="button">{{$slider['button_text']}}</a>
                    @endisset
                </div>
            @endforeach
{{--end sliders from db--}}
            </div>
        </div>
        <div class="animation">
            <div class="diamond2"></div>
            <div class="diamond"></div>
            <div class="cardio"></div>
        </div>
    </div>
</section>
@endisset
@isset($data['about_items'])
<section class="main-about-us" id="about-ton-us">
    <div class="container">
        <div class="headline">
            <div class="main">{{__('strings.about Free TON')}}</div>
            <div class="shadowed">{{__('strings.about us')}}</div>
        </div>
        <div class="tab-list">
{{--anout_items from db--}}
        @foreach($data['about_items'] as $about)
            <div class="item">
                <a href="#about-ton-us-{{$loop->iteration}}"
                @if($loop->first)
                    class="link active"
                @else
                   class="link"
                @endif
                >{{$about['name']}}</a>
            </div>
        @endforeach
        </div>
        @foreach($data['about_items'] as $about)
        <div class="tab-content" id="about-ton-us-{{$loop->iteration}}">
            <div class="text">
                <p>
                    {{$about['content']}}
                </p>
            </div>
            @isset($about['video_url'])
            <div class="video">
                <iframe src={{$about['video_url']}}></iframe>
{{--                <a class="play-button"></a>--}}
            </div>
            @endisset
        </div>
        @endforeach
{{--end anout_items from db--}}
    </div>
</section>
@endisset
@isset($data['markets'])
<section class="main-markets" id="markets">
    <div class="container">
        <div class="headline">
            <div class="main">{{__('strings.markets')}}</div>
            <div class="shadowed">{{__('strings.markets')}}</div>
        </div>
        <div class="mobile-table">
{{--markets from db--}}
        @foreach($data['markets'] as $market)
            <div class="market">
                <b>{{__('strings.market')}}</b>
                <a href="{{$market['url']}}" target="_blank">
                    <img src={{$market['logo_path']}} alt="{{$market['name']}}">
                    {{$market['name']}}
                </a>
            </div>
            <table>
                <tr>
                    <td>{{__('strings.ask')}}</td>
                    <td>{{__('strings.bid')}}</td>
                    <td>{{__('strings.volume')}} 24 h</td>
                </tr>
                <tr>
                    <td>{{number_format($market['ask'], 2, ",", " ")}} USDT</td>
                    <td>{{number_format($market['bid'], 2, ",", " ")}} USDT</td>
                    <td>{{number_format($market['volume'], 0, ",", " ")}} USDT</td>
                </tr>
            </table>
        @endforeach
{{--end markets from db--}}
        </div>
        <table class="desktop-table">
            <thead>
            <tr>
                <td>{{__('strings.market')}}</td>
                <td>{{__('strings.ask')}}</td>
                <td>{{__('strings.bid')}}</td>
                <td>{{__('strings.volume')}} 24 h</td>
            </tr>
            </thead>
{{--markets from db--}}
            @foreach($data['markets'] as $market)
            <tr>
                <td>
                    <a href="{{$market['url']}}" target="_blank">
                        <img src={{$market['logo_path']}} alt="{{$market['name']}}">
                        {{$market['name']}}
                    </a>
                </td>
                <td>
                    {{number_format($market['ask'], 2, ",", " ")}} USDT
                </td>
                <td>
                    {{number_format($market['bid'], 2, ",", " ")}} USDT
                </td>
                <td>
                    {{number_format($market['volume'], 0, ",", " ")}} USDT
                </td>
            </tr>
            @endforeach
{{--end markets from db--}}
        </table>
    </div>
</section>
@endisset
@isset($data['news_items'])
<section class="main-news">
    <div class="container">
        <div class="headline">
            <div class="main">{{__('strings.news')}}</div>
            <div class="shadowed">{{__('strings.news')}}</div>
        </div>
        <div class="news-list">
{{--news_items from db--}}
            @foreach($data['news_items'] as $item)
            <div class="item">
                <div class="single">
                    <a href="{{$data['locale'].'/news/'.$item['url']}}" class="link"></a>
                    <span class="title">
                    {{$item['title']}}
                </span>
                    <img src="{{$item['img_path']}}" alt="{{$item['title']}}" class="image">
                    <div class="date">{{$item['updated_at']}}</div>
                </div>
            </div>
            @endforeach
{{--end news_items from db--}}
        </div>
    </div>
</section>
@endisset
@isset($data['partners'])
<section class="main-partners" id="partners">
    <div class="container">
        <div class="headline">
            <div class="main">{{__('strings.partners')}}</div>
            <div class="shadowed">{{__('strings.partners')}}</div>
        </div>
        <div class="partners-list">
{{--news_items from db--}}
            @foreach($data['partners'] as $partner)
            <div class="item">
                <a href="{{$partner['url']}}" target="_blank">
                    <img src={{$partner['logo_path']}} alt="{{$partner['name']}}">
                </a>
            </div>
            @endforeach
{{--end news_items from db--}}
        </div>
    </div>
</section>
@endisset
@isset($data['useful_links'])
<section class="main-links" id="useful_links">
    <div class="container">
        <div class="headline">
            <div class="main">{{__('strings.useful info')}}</div>
            <div class="shadowed">{{__('strings.useful info')}}</div>
        </div>
{{--news_items from db--}}
        @foreach($data['useful_links'] as $chunk)
            <div class="links">
            @foreach($chunk as $link)
                <a href="{{$link['url']}}" target="_blank">- {{$link['name']}}</a>
            @endforeach
            </div>
        @endforeach
{{--end news_items from db--}}
    </div>
</section>
@endisset
@include('includes/footer')
</body>
</html>
