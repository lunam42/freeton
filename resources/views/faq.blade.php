<!doctype html>
<html lang="{{$data['locale']}}">
@include('includes/head', ['data' => $data['page'], 'main' => false])
@include('includes/header_menu', ['menu' => $data['menu'],'langs' => $data['langs'], 'locale' => $data['locale'], 'main' => false])
<body>
<section class="inner-content">
    <div class="container">
        <div class="pages-tree">
            <a href="{{url("/{$data['locale']}")}}">{{__('strings.home')}}</a>
            <a href="{{url("/{$data['locale']}/faq")}}">{{__('strings.FAQ')}}</a>
            {{$data['faq']['active']}}
        </div>
        <div class="headline">
            <div class="main">{{$data['page']['headline']}}</div>
            <div class="shadowed">{{$data['page']['headline']}}</div>
        </div>
        <div class="faq-topics">
            @foreach($data['faq']['sections'] as $section)
            <div class="item">
                @if($section['name'] == $data['faq']['active'])
                <a href={{$section['url']}} class="active">{{$section['name']}}</a>
                @else
                <a href={{$section['url']}}>{{$section['name']}}</a>
                @endif
            </div>
            @endforeach
        </div>
        @foreach($data['faq']['items'] as $item)
        <div class="faq-item">
            <div class="top">{{$item['question']}}</div>
            <div class="bot">{{$item['answer']}}</div>
        </div>
        @endforeach
{{--        <div class="pager">--}}
{{--            <a class="active">1</a>--}}
{{--            <a href="#">2</a>--}}
{{--        </div>--}}
    </div>
</section>
@include('includes/footer')
</body>
</html>
