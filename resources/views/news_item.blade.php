
<!doctype html>
<html lang="{{$data['locale']}}">
@include('includes/head', ['data' => $data['page'], 'main' => false])
@include('includes/header_menu', ['menu' => $data['menu'],'langs' => $data['langs'], 'locale' => $data['locale'], 'main' => false])
<body>
<section class="inner-content">
    <div class="container">
        <div class="pages-tree">
            <a href="{{url("/{$data['locale']}")}}">{{__('strings.home')}}</a>
            <a href="{{url("/{$data['locale']}/{$data['category']}")}}">{{__('strings.news')}}</a>
            {{$data['page']['title']}}
        </div>
        <div class="headline">
            <div class="main">{{$data['page']['title']}}</div>
{{--            <div class="shadowed">{{$data['page']['title']}}</div>--}}
        </div>
        <div class="news-content">
            <img src="{{$data['page']['img_path']}}" alt="{{$data['page']['title']}}" style="width: 200px;">
            <p><span class="date">{{$data['page']['updated_at']}}</span></p>
            <p>
                <b>{{$data['page']['title']}}</b><br>
                {{$data['page']['description']}}
            </p>
            <p>
                {{$data['page']['content']}}
            </p>
        </div>
    </div>
</section>
@include('includes/footer')
</body>
</html>
