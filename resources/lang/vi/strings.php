<?php

return [
    'about Free TON' => 'Giới thiệu về Free TON',
    'about us' => 'Về chúng tôi',
    'markets' => 'Thị trường',
    'market' => 'Thị trường',
    'partners' => 'Đối tác',
    'useful info' => 'Thư viện',
    'news' => 'Tin tức',
    'FAQ' => 'Hỏi - Đáp',
    'ask' => 'Bán',
    'bid' => 'Mua',
    'volume' => 'Khối lượng giao dịch',
    'home' => 'Trang chủ',
    'copyright' => 'Bản quyền',
];
