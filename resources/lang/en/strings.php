<?php

return [
    'about Free TON' => 'about Free TON',
    'about us' => 'about us',
    'markets' => 'markets',
    'market' => 'market',
    'partners' => 'partners',
    'useful info' => 'useful info',
    'news' => 'news',
    'FAQ' => 'FAQ',
    'ask' => 'ask',
    'bid' => 'bid',
    'volume' => 'volume',
    'home' => 'home',
    'copyright' => 'copyright',
];
