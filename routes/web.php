<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\EnterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '{locale}',
              'where' => ['locale' => '[a-z]{2}'],
              'middleware' => 'existence'], function() {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/{category}', [HomeController::class, 'category']);
    Route::get('/{category}/{item}', [HomeController::class, 'items']);
});
Route::group(['prefix' => config('constants.admin_path'),
              'middleware' => 'auth'], function() {
    Route::match(['get', 'post'], '/', [EnterController::class, 'login'])->name('enter')->withoutMiddleware('auth');
    Route::get('/dashboard', function() {
        dd('dashboard');
    })->name('dashboard');
    Route::get('/test', function() {
        dd('test');
    });
});
Route::get('/{any}')->where('any', '.*')->middleware('locale');



