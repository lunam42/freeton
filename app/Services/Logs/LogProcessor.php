<?php
namespace App\Services\Logs;

class LogProcessor
{
    public function __invoke(array $record)
    {
        $record['extra'] = ['request_from' => "ip: " . request()->server('REMOTE_ADDR')];
        return $record;
    }
}
