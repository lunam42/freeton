<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalData extends Model
{
    use HasFactory;

    protected $fillable = ['key', 'value', 'comment'];
    protected $table = 'total_data';

    public function getValueAttribute($value)
    {
        $res = json_decode($value);
        if (!$res)
            return $value;
        return $res;
    }

    public function setValueAttribute($value)
    {
        if (is_object($value) || is_array($value)) {
            $this->attributes['value'] = json_encode($value);
        } else {
            $this->attributes['value'] = $value;
        }
    }

    /**
     * Возвращает значение(я) поля(ей) value по ключу key.
     *
     * @param string $key
     * @param $default - значение по умолчанию
     * @return string|array
     * @throws \Exception - если в базе нет ключа и default не установлен - ошибка
     */
    static public function val($key, $default = null)
    {
        $data = self::where('key', $key)->first();
        if ($data === null) {
            if ($default === null)
                throw new \Exception("'$key' not set in TotalData");
            else
                return $default;
        }
        return $data->value;
    }

    /**
     * Возвращает значения полей value по ключу key.
     *
     * @param string $key
     * @param array|null $default - значение по умолчанию
     * @return string|array
     * @throws \Exception - если в базе нет ключа и default не установлен - вернет []
     */
    static public function vals($key, $default = null)
    {
        $data = self::where('key', $key)->get();
        if (!$data->count()) return $default === null ? [] : $default;
        return $data->pluck('value')->toArray();
    }
}
