<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $casts = ['context' => 'array'];

    public function setTraceAttribute(?array $value)
    {
        if (!$value) $this->attributes['trace'] = null;
        else $this->attributes['trace'] = implode("\n", $value);
    }

    public function getTraceAttribute(array $value): ?array
    {
        if (!$value) return null;
        return explode("\n", $value);
    }
}
