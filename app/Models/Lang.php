<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lang extends Model
{
    use HasFactory;

    protected $fillable = ['code', 'flag_path'];

    public function news ()
    {
        return $this->hasMany(News::class);
    }
}
