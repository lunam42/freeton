<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FAQ_Item extends Model
{
    use HasFactory;

    protected $fillable = ['faq_section_id', 'question', 'answer'];
    protected $table = 'faq_items';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section ()
    {
        return $this->belongsTo(FAQ_Section::class);
    }
}
