<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;

    protected $fillable = ['lang_id', 'name', 'content', 'video_url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lang ()
    {
        return $this->belongsTo(Lang::class);
    }

    /**
     * Scope a query to only include menus of a given lang
     * @param  \Illuminate\Database\Eloquent\Builder    $query
     * @param  string                                   $code
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfLang($query, $code)
    {
        return $query->whereHas('lang', function($query) use ($code) {
            $query->where('code', $code);
        });
    }
}
