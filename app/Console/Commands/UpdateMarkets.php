<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Market;

class UpdateMarkets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'markets_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $markets = Market::whereNotNull('class')->get();
        foreach ($markets as $market) {
            $class = $market->class;
            $res = $class::takeByApi();
            if (!$res)
                continue;
            $market->update($res);
        }
    }
}
