<?php

namespace App\Classes;

use Illuminate\Support\Facades\Http;

class Hitbtc
{
    const URL = 'https://api.hitbtc.com/api/2/public/ticker?symbols=TONUSD';

    public static function takeByApi ()
    {
        $res = Http::get(self::URL);
        if (!$res->successful())
            return false;
        $info = head($res->json());
        return ['bid' => $info['bid'], 'ask' => $info['ask'], 'volume' => $info['volume']];
    }
}
