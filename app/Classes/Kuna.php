<?php

namespace App\Classes;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class Kuna
{
    const URL = 'https://api.kuna.io/v3/tickers?symbols=TONUSDT';

    public static function takeByApi ()
    {
        $res = Http::get(self::URL);
        if (!$res->successful())
            return false;
        $info = Arr::flatten($res->json());
        return ['bid' => $info[1], 'ask' => $info[3], 'volume' => $info[8]];
    }
}
