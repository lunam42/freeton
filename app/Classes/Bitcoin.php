<?php

namespace App\Classes;

use Illuminate\Support\Facades\Http;

class Bitcoin
{
    const URL = 'https://api.exchange.bitcoin.com/api/2/public/ticker/TONUSD';

    public static function takeByApi ()
    {
        $res = Http::get(self::URL);
        if (!$res->successful())
            return false;
        $info = $res->json();
        return ['bid' => $info['bid'], 'ask' => $info['ask'], 'volume' => $info['volume']];
    }
}
