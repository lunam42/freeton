<?php

namespace App\Http\Controllers;

use App\Models\FAQ_Section;
use Illuminate\Http\Request;
use App\Models\About;
use App\Models\Lang;
use App\Models\Market;
use App\Models\Menu;
use App\Models\News;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Slider;
use App\Models\Useful_link;

class HomeController extends Controller
{
    public function index(Request $request, $locale)
    {
        app()->setLocale($locale);
        $data['langs'] = Lang::all()->toArray();
        $data['page'] = Page::ofLang($locale)->where('url', '/')->first()->toArray();
        $data['locale'] = $locale;
        $data['menu'] = $this->takeMenuItems($locale);
        $data['slider_items'] = Slider::ofLang($locale)->get()->toArray() ? : null;
        $data['about_items'] = About::ofLang($locale)->get()->toArray() ? : null;
        $data['markets'] = Market::all()->filter(function($val, $key) {
            return $val->ask;
        })->toArray() ? : null;
        $data['news_items'] = News::ofLang($locale)->latest()->take(6)->get()->toArray() ? : null;
        $data['partners'] = Partner::all()->toArray() ? : null;
        $data['useful_links'] = Useful_link::ofLang($locale)->get()->chunk(6)->toArray() ? : null;
        return view('home', ['data' => $data]);
    }

    public function items (Request $request, $locale, $category, $item)
    {
        app()->setLocale($locale);
        $data['menu'] = $this->takeMenuItems($locale);
        $data['langs'] = Lang::all()->toArray();
        $data['locale'] = $locale;
        switch ($category) {
            case 'faq':
                $data['faq'] = $this->takeFaqData($locale, $item);
                $data['page'] = Page::ofLang($locale)->where('url', 'faq')->first()->toArray();
                $view = 'faq';
            break;
            case 'news':
                $data['page'] = News::ofLang($locale)->where('url', $item)->first()->toArray();
                $data['category'] = $category;
                $view = 'news_item';
            break;
        }
        return view($view, ['data' => $data]);
    }

    public function category (Request $request, $locale, $category)
    {
        app()->setLocale($locale);
        $data['menu'] = $this->takeMenuItems($locale);
        $data['langs'] = Lang::all()->toArray();
        $data['locale'] = $locale;
        $data['page'] = Page::ofLang($locale)->where('url', $category)->first()->toArray();
        switch ($category) {
            case 'faq':
                $data['faq'] = $this->takeFaqData($locale);
                $view = $category;
            break;
            case 'news':
                $data['news'] = News::ofLang($locale)->get()
                                                     ->transform(function($item, $key) use ($locale) {
                                                         $item->url = url()->current().'/'.$item->url;
                                                         return $item;
                                                     })->toArray();
                $view = $category;
            break;
            default:
                $view = 'page';
        }
        return view($view, ['data' => $data]);
    }

    private function takeMenuItems ($locale)
    {
        return Menu::ofLang($locale)->orderBy('sequence', 'asc')
                                    ->get()
                                    ->transform(function($item, $key) use ($locale) {
                                        $res['blank'] = $item->blank;
                                        $res['name'] = $item->name;
                                        $res['url'] = in_array($item->url[0], ['#', '/']) ?
                                            url($locale.$item->url) : $item->url;
                                        return $res;
                                    })->toArray();
    }

    private function takeFaqData ($locale, $item = null)
    {
        $sections = FAQ_Section::ofLang($locale)->orderBy('sequence', 'asc')->get();
        if (!$item) {
            $active = $sections->first();
            $items = $active->items;
            $sections = $sections->transform(function($val, $key) use ($locale) {
                $val->url = url()->current().'/'.$val->url;
                return $val;
            });
        } else {
            $active = $sections->first(function($val, $key) use ($item) {
                return $val->url == $item;
            });
            $items = $active->items;
        }
        $active = $active->name;
        return ['sections' => $sections->toArray(),
                'items' => $items->toArray(),
                'active' => $active];
    }
}
