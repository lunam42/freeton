<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Lang;

class Locale
{
    /**
     * catch all incoming request, except starting with [a-z]{2}, detect & prepend locale & redirect
     * @param  \Illuminate\Http\Request     $request
     * @param  \Closure                     $next
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $full_locale = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $locale = substr($full_locale, 0, 2);
        if (!Lang::where('code', $locale)->first())
            $locale = config('app.fallback_locale');
        if (empty($request->segments()))
            return redirect($locale);
        if (count($request->segments()) > 3)
            abort(404);
        return redirect($locale.'/'.$request->path());
    }
}
