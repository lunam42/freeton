<?php

namespace App\Http\Middleware;

use Closure;
use App\Facades\Log;

class LogRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $path = $request->path();
	    Log::info("Request to: '$path'", $request->all());
	    return $next($request);
    }
}
