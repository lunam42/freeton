<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Lang;

class Existence
{
    /**
     * do we change url
     * @var bool
     */
    private $corrected;

    /**
     * current detected locale
     * @var Lang
     */
    private $locale;

    /**
     * @var Lang
     */
    private $fallback_lang;

    /**
     * requested url's segment after locale
     * @var string
     */
    private $page;

    /**
     * requested url's segment after segment $page
     * @var string
     */
    private $item;

    /**
     * detect if requested resources exist
     * @param  \Illuminate\Http\Request     $request
     * @param  \Closure                     $next
     * @return \Illuminate\Http\RedirectResponse | \Closure
     */
    public function handle(Request $request, Closure $next)
    {
        $this->fallback_lang = Lang::where('code', config('app.fallback_locale'))->first();
        $this->locale = Lang::where('code', $request->route()->parameter('locale'))->first();
        $this->corrected = false;
        if (!$this->locale) {
            $this->corrected = true;
            $this->locale = $this->fallback_lang;
        }
        $this->page = $request->route()->parameter('category');
        $this->item = $request->route()->parameter('item');
        if (!$this->check())
            abort(404);
        if ($this->corrected) {
            return redirect($this->locale->code.'/'.$this->page.'/'.$this->item);
        }
        return $next($request);
    }

    /**
     * check if exist requested resource (page or directory-page & directory-items)
     * @return bool
     */
    private function check ()
    {
        if (!$this->item)
            return $this->checkResource(false);
        return $this->checkResource(false) && $this->checkResource(true);
    }

    /**
     * service function for check()
     * @param $is_item  bool : true (to check child resource of 'directory') | false (to check 'directory')
     * @return bool
     */
    private function checkResource ($is_item)
    {
        $table = $is_item ? ($this->page == 'news' ? 'news' : 'faq_sections') : 'pages';
        $what = $is_item ? $this->item : ($this->page ? : '/');
        if (DB::table($table)->where([['lang_id', '=', $this->locale->id], ['url', '=', $what]])->exists())
            return true;
        if ($this->locale->id != $this->fallback_lang->id &&
            DB::table($table)->where([['lang_id', '=', $this->fallback_lang->id],
                                      ['url', '=', $what]])->exists()) {
            $this->locale = $this->fallback_lang;
            $this->corrected = true;
            return true;
        }
        return false;
    }
}
