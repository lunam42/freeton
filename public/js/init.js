$(document).ready(function() {
    $(window).scroll(function(){
        var sticky = $('.header'),
            scroll = $(window).scrollTop();

        if (scroll >= 1) sticky.addClass('white');
        else sticky.removeClass('white');
    });

    $('.inner-content .faq-item .top').click(function() {
        $(this).toggleClass('opened');
        $(this).next('.inner-content .faq-item .bot').slideToggle();
    });

    $('.header .mobile-menu-button').click(function() {
        $('.mobile-menu-overlay').fadeIn();
        $('.mobile-menu-body').fadeIn();
    });
    $('.mobile-menu-body .close').click(function() {
        $('.mobile-menu-overlay').fadeOut();
        $('.mobile-menu-body').fadeOut();
    });
    $('.mobile-menu-overlay').click(function() {
        $('.mobile-menu-overlay').fadeOut();
        $('.mobile-menu-body').fadeOut();
    });

    $('.main-about-us .tab-content').hide();
    $('.main-about-us .tab-content:first').show();
    $('.main-about-us .tab-list .item:first .link').addClass('active');
    $('.main-about-us .tab-list .item .link').click(function() {
        $('.main-about-us .tab-list .item .link').removeClass('active');
        $(this).addClass('active');
        $('.main-about-us .tab-content').hide();
        var activeExamTab = $(this).attr('href');
        $(activeExamTab).fadeIn();
        return false;
    });

    $('.main-banner-slider').owlCarousel({
        loop:true,
        items:1,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true
    });
});